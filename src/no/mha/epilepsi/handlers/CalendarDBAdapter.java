package no.mha.epilepsi.handlers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * This class will create a database for the date registered in the calendar
 * @author Adiljan
 *
 */
public class CalendarDBAdapter {

		public static final String KEY_DATE = "date";
	    public static final String KEY_CHOICE ="choice";
	    public static final String KEY_TIME = "time";
	    public static final String KEY_ROWID = "_id";

	    private static final String TAG = "NDBAdapter";
	    private DatabaseHelper mDbHelper;
	    private SQLiteDatabase mDb;

	    /**
	     * Database creation sql statement
	     */
	    private static final String DATABASE_CREATE =
	        "create table calendar (_id integer primary key autoincrement, "
	        + "date text not null, choice text not null, time text not null);";

	    private static final String DATABASE_NAME = "info";
	    private static final String DATABASE_TABLE = "calendar";
	    private static final int DATABASE_VERSION = 2;

	    private final Context mCtx;

	    private static class DatabaseHelper extends SQLiteOpenHelper {

	        DatabaseHelper(Context context) {
	            super(context, DATABASE_NAME, null, DATABASE_VERSION);
	        }

	        @Override
	        public void onCreate(SQLiteDatabase db) {

	            db.execSQL(DATABASE_CREATE);
	        }

	        @Override
	        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
	                    + newVersion + ", which will destroy all old data");
	            db.execSQL("DROP TABLE IF EXISTS throws");
	            onCreate(db);
	        }
	    }

	    /**
	     * Constructor - takes the context to allow the database to be
	     * opened/created
	     * 
	     * @param ctx the Context within which to work
	     */
	    public CalendarDBAdapter(Context ctx) {
	        this.mCtx = ctx;
	    }

	    /**
	     * Open the calendars database. If it cannot be opened, try to create a new
	     * instance of the database. If it cannot be created, throw an exception to
	     * signal the failure
	     * 
	     * @return this (self reference, allowing this to be chained in an
	     *         initialization call)
	     * @throws SQLException if the database could be neither opened or created
	     */
	    public CalendarDBAdapter open() throws SQLException {
	        mDbHelper = new DatabaseHelper(mCtx);
	        mDb = mDbHelper.getWritableDatabase();
	        return this;
	    }

	    public void close() {
	        mDbHelper.close();
	    }


	    /**
	     * Create a new calendar using the title and body provided. If the calendar is
	     * successfully created return the new rowId for that calendar, otherwise return
	     * a -1 to indicate failure.
	     * 
	     * @param title the title of the calendar
	     * @param body the body of the calendar
	     * @return rowId or -1 if failed
	     */
	    public long createDate(String date, String choice, String time) {
	        ContentValues initialValues = new ContentValues();
	        initialValues.put(KEY_DATE, date);
	        initialValues.put(KEY_CHOICE, choice);
	        initialValues.put(KEY_TIME, time);

	        return mDb.insert(DATABASE_TABLE, null, initialValues);
	    }

	    /**
	     * Delete the calendar with the given rowId
	     * 
	     * @param rowId id of calendar to delete
	     * @return true if deleted, false otherwise
	     */
	    public boolean deleteDate(long rowId) {

	        return mDb.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
	    }

	    /**
	     * Return a Cursor over the list of all calendars in the database
	     * 
	     * @return Cursor over all calendars
	     */
	    public Cursor fetchAllDates() throws SQLException {

	        return mDb.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_DATE,
	                KEY_CHOICE, KEY_TIME}, null, null, null, null, null);
	    }

	    /**
	     * Return a Cursor positioned at the calendar that matches the given rowId
	     * 
	     * @param rowId id of calendar to retrieve
	     * @return Cursor positioned to matching calendar, if found
	     * @throws SQLException if calendar could not be found/retrieved
	     */
	    public Cursor fetchDate(long rowId) throws SQLException {

	        Cursor mCursor = mDb.query(true, DATABASE_TABLE, new String[] {KEY_ROWID,
	        		KEY_DATE, KEY_CHOICE, KEY_TIME}, KEY_ROWID + "=" + rowId, null,
	                    null, null, null, null);
	        if (mCursor != null) {
	            mCursor.moveToFirst();
	        }
	        return mCursor;

	    }

	    /**
	     * Update the calendar using the details provided. The calendar to be updated is
	     * specified using the rowId, and it is altered to use the title and body
	     * values passed in
	     * 
	     * @param rowId id of calendar to update
	     * @param date value to set calendars date to
	     * @param choice value to set calendar choice to
	     * @return true if the calendar was successfully updated, false otherwise
	     */
	    public boolean updateDate(long rowId, String date, String choice, String time) {
	        ContentValues args = new ContentValues();
	        args.put(KEY_DATE, date);
	        args.put(KEY_CHOICE, choice);
	        args.put(KEY_TIME, time);

	        return mDb.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
	    }
	}