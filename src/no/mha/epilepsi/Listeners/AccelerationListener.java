/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package no.mha.epilepsi.Listeners;

//import android.hardware.SensorListener;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;

/**
 *
 * @author hallvardwestman
 */
public class AccelerationListener implements SensorEventListener {
    
private SensorManager sensorManager;
    private static float initiatedEvent;
    
    private Bundle curAccel;
    private Handler handler;
    boolean running = true;
    boolean accelLock = true;
    
    
    /*
     * sets sensormanager from activity
     */
    public AccelerationListener(SensorManager sm){
        curAccel = new Bundle();
        sensorManager = sm;
        
    }
    
    /*
     * Initializes the sensor listening.
     */
    public void startAccelerationListening() {
        
        
    	sensorManager.registerListener(this, 
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);//
        
        
    }
    /*
     * Unregisters the listener
     */
    public void stopListening() {
         sensorManager.unregisterListener(this);
     }
    
    /*
     * sets current accelerometer-data
     */
    public void onSensorChanged(final SensorEvent event) {
        
        int sensorType = event.sensor.getType();
        
       if (sensorType == Sensor.TYPE_ACCELEROMETER) {
            
            
            updateAccel(event.values[0], event.values[1], event.values[2]);
         
        }
    }
    
    /*
     * Never in use
     */
    public void onAccuracyChanged(Sensor arg0, int arg1) {
       //   throw new UnsupportedOperationException("Not supported yet.");
    }
    
    
    /*
     * returns accelerometer-data when polled
     */
    public Bundle returnAcceleration(){
        return curAccel;
        
    }
    
    public void updateAccel(float x,float y,float z){
        
        curAccel = new Bundle();
        curAccel.putFloat("x", x);
        curAccel.putFloat("y", y);
        curAccel.putFloat("z", z);
        
    }
}