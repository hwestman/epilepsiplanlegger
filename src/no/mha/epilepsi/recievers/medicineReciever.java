package no.mha.epilepsi.recievers;

import no.mha.epilepsi.R;
import no.mha.epilepsi.Activities.DisplayReminder;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class medicineReciever extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		 	String notificationId = (intent.getData().getSchemeSpecificPart().toString());

		    // Raise the notification so that user can check the details
		    NotificationManager mNotificationManager = (NotificationManager) context
		            .getSystemService(Context.NOTIFICATION_SERVICE);

		    int icon = R.drawable.medicines;
		    CharSequence tickerText = "Medisin";
		    long when = System.currentTimeMillis();

		    Notification notification = new Notification(icon, tickerText, when);

		    // Count of number of notifications
		    notification.number = 1;

		    CharSequence contentTitle = "Ta medisinen din!";
		    CharSequence contentText = intent.getStringExtra("dose")+" stk av "+intent.getStringExtra("name");

		    // The PendingIntent to launch our activity if the user selects this
		    // notification
		    Intent notificationIntent = new Intent(context, DisplayReminder.class);
		    notificationIntent.putExtra("dose", intent.getStringExtra("dose"));
		    notificationIntent.putExtra("name", intent.getStringExtra("name"));
		    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		    PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
		            notificationIntent, 0);


		    // Set the info for the views that show in the notification panel.
		    notification.setLatestEventInfo(context, contentTitle, contentText,
		            contentIntent);
		    notification.defaults |= Notification.DEFAULT_SOUND;
		    notification.defaults |= Notification.DEFAULT_VIBRATE;
		    notification.defaults |= Notification.DEFAULT_LIGHTS;
		    int flagInsistent = Notification.FLAG_INSISTENT;

		    // Instead of 1234 or any other number, use below expression to have unique notifications
		    // Integer.parseInt(intent.getData().getSchemeSpecificPart())
		    mNotificationManager.notify(Integer.parseInt(intent.getData().getSchemeSpecificPart()), notification);
		    context.startActivity(notificationIntent);
	}
	
}
