/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package no.mha.epilepsi.Activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import no.mha.epilepsi.R;
import no.mha.epilepsi.services.Monitor;

/**
 *
 * @author hallvardwestman
 */
public class WarnContact extends Activity {

    
    public static int CONTACT_ALERT_LIMIT = 5;
    static PendingIntent pi;
    static WarnContact warnContact;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.warncontact);
        warnContact = this;
        
        alert();

        sendSMS();

        Log.d("WarnContact", "ran alert");
        
        
        Button userOk= (Button) findViewById(R.id.user_ok);
        userOk.setOnClickListener(new OKListener());
        
        pi = PendingIntent.getActivity(this, 0,new Intent(this, WarnContact.class), 0); 
        alertContactTimer();
        

    }
    
    private static class OKListener implements OnClickListener {

        public void onClick(View arg0) {
            Monitor.stopMonitor();
            warnContact.finish();
        }
    }
    
    
     public static void alertContactTimer(){
        
         Thread t = new Thread(new Runnable() {
            
            int timeLimit =0;
            
            public void run() {
                    
                    while(EpilepsiPlanlegger.MONITOR_RUNNING == true && timeLimit< CONTACT_ALERT_LIMIT){
                        
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ex) {

                        }
                        timeLimit++;

                        }

                        if(timeLimit == CONTACT_ALERT_LIMIT && EpilepsiPlanlegger.MONITOR_RUNNING == true){
                            alertContactHandler.sendEmptyMessage(1);
                        }
            }
         });
         t.start();
    }
    /*
     * handles the alertTimer thread
     */
    
    static Handler alertContactHandler = new Handler(){
       
        @Override
        public void handleMessage(Message msg) {
            /*
             * should store monitor as if wer done
             */
            alert();
            Log.d("WarnContact", "ran alert");
            /*
             * continues alerting contactperson with new location
             * aslong as it runs
             */
            alertContactTimer();;
        
        }
    };
    
    public static void alert(){
        EpilepsiPlanlegger.getInstanceOf().alarmUser("Your contactPerson has been alerted of your situation");
        Log.d("Monitor : ","ALERT HAS BEEN INITIATED");

         
        
    }
    
    
    
    public static void sendSMS(){        
                       
        SmsManager sms = SmsManager.getDefault();
        
        String ContactPerson = "95935336";
        StringBuilder sb = new StringBuilder();
        
        sb.append("<NAMEOFUSER> is not responding at this location : ");
        //sb.append
        
        sb.append(Monitor.getInstanceOf().seizureHandler.lastReading.getLocation().toString());
        //sb.append(Monitor.seizureHandler.getLocation().toString());
        Log.d("WTFISTHIS", sb.toString());
        String s = sb.toString();
        
        if(Monitor.CONTACT_NUMBER != "")
        sms.sendTextMessage(Monitor.CONTACT_NUMBER, null,s, pi, null);   
        else 
            sms.sendTextMessage("97428808", null,s, pi, null); 

    }
    @Override
    public void onBackPressed (){}
    
    
}
