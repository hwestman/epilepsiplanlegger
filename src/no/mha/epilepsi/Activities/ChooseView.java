package no.mha.epilepsi.Activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import no.mha.epilepsi.R;
import no.mha.epilepsi.Listeners.CustomOnItemSelectedListener;
import no.mha.epilepsi.handlers.CalendarDBAdapter;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Class ChooseView is responsible for storing data after final choose.
 * The choose of date, time have to be done before getting here.
 * Xtra: ChooseView class can be accessed irectly through main activity by automatic defined time and date.
 * @author Adiljan
 *
 */
public class ChooseView extends Activity {

	  private Spinner spinner1, spinner2;
	  private Button btnSubmit, btnEdit;
	  private String day;
	  private String time;
	  private String choice;
	  protected boolean exist;
	  private int Rid;
	  Vector<CollectData> all;
	  CollectData cd; 
	  CalendarDBAdapter db;
	 
	  /**
	   * Main method that will initiate all necessary operation and setting up values from other intent.
	   * Here we initiate check of database for existing data.
	   */
		@Override
		public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_view);
		all = new Vector<CollectData>();
		db = new CalendarDBAdapter(this);
		
		addItemsOnSpinner2();
		addListenerOnButton();
		addListenerOnSpinnerItemSelection();
		
		Bundle extras = getIntent().getExtras();
    	day = extras.getString("day");
    	time = extras.getString("time");
    	
    	String[] data= time.split(" ");
    	time = data[0];
    	
    	checkRegistered(data[0]);
    	    	
	  }
	 
		/**
		 *  Add items into spinner n:2 dynamically
		 */
		public void addItemsOnSpinner2() {
	 
		spinner2 = (Spinner) findViewById(R.id.spinner2);
		List<String> list = new ArrayList<String>();
		list.add("Choose seizure type");
		list.add("Seizure type1");
		list.add("Seizure type2");
		list.add("Seizure type3");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
			android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		spinner2.setAdapter(dataAdapter);
	  }
	 
	 /**
	  * Spinner N:1 is added through use of menu in res folder.
	  * See choose.xml for more details and string.xml
	  */
	  public void addListenerOnSpinnerItemSelection() {
		spinner1 = (Spinner) findViewById(R.id.spinner1);
		spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());
	  }
	 
	  /**
	   *  Gets the selected drop-down list value
	   */
	  public void addListenerOnButton() {
	 
		spinner1 = (Spinner) findViewById(R.id.spinner1);
		spinner2 = (Spinner) findViewById(R.id.spinner2);
		btnSubmit = (Button) findViewById(R.id.btnSubmit);
			 
		btnSubmit.setOnClickListener(new OnClickListener() {
	 
		/**
		 * Output on the click of submit button. 
		 * Here we have a check if data inserted exist in DB. If not it will be created otherwise updated if  changes are made.
		 */
	   @Override
	   public void onClick(View v) {
	 
		    Toast.makeText(ChooseView.this,
			"Submited values : " + 
	                "\nSpinner 1 : "+ String.valueOf(spinner1.getSelectedItem()) + 
	                "\nSpinner 2 : "+ String.valueOf(spinner2.getSelectedItem()) +
	                "\nRegistered: "+day+" - "+time ,
				Toast.LENGTH_SHORT).show();
		            	   
        	if (String.valueOf(spinner1.getSelectedItem()).equals("Choose condition"))
        		choice=String.valueOf(spinner2.getSelectedItem());
        	else if (String.valueOf(spinner2.getSelectedItem()).equals("Choose seizure type"))
        		choice=String.valueOf(spinner1.getSelectedItem());
        	else
        		choice=String.valueOf(spinner1.getSelectedItem())+"-"+ String.valueOf(spinner2.getSelectedItem());
        	
            db.open();        
            
            long id;
            boolean d=false;
            
            if (exist==true)
            	d=db.updateDate(Rid, day, choice, time);
            else
            	id = db.createDate(day, choice, time);
            
	      	db.close();
            
            Intent tv = new Intent(ChooseView.this, EpilepsiPlanlegger.class);
            startActivity(tv);
		  }
	 
		});
	  }
	  
	  /**
	   * Checks for all DB entries to check if the date according to the given value exists.
	   * If data exists it will be parsed to the spinner1 and 2.
	   * @param time time
	   */
	  public void checkRegistered(String time){
			CalendarDBAdapter db = new CalendarDBAdapter(this);
			db.open();
		 	Cursor cursor;
	        cursor = db.fetchAllDates();
	      	String [] choice = new String[cursor.getCount()];
	      	
	      	int i=0;	  
	      	if (cursor.moveToFirst())
 	        {
 		      do {
 		    	 if(cursor.getString(1).equals(day)){
   		    		 if(cursor.getString(3).equals(time))
   		    		 {
   		    			 choice[i] = cursor.getString(2);
   		    			 exist = true;
   		    			 Rid = i+1;
   		    			 String[] condition = choice[i].split("-");
   		    			 spinner1 = (Spinner) findViewById(R.id.spinner1);
   		    			 if (condition[0].equals("Happy"))
   		    			 {
   		    				 spinner1.setSelection(1);
   		    			 }
   		    			 else if (condition[0].equals("Sad"))
  		    			 {
  		    				 spinner1.setSelection(2);
  		    			 }
   		    			 else if (condition[0].equals("Bored"))
   		    			 {
   		    				 spinner1.setSelection(3);	 
   		    			 }
   		    			 else if (condition[0].equals("Frustrated"))
   		    			 {
   		    				 spinner1.setSelection(4);
   		    			 }
   		    			 else if (condition[0].equals("Angry"))
  		    			 {
  		    				 spinner1.setSelection(5);	 
  		    			 }
   		    			 else if (condition[0].equals("Alone"))
   		    			 {
   		    				 spinner1.setSelection(6);
   		    			 }
   		    			 else if (condition[0].equals("Freaked"))
  		    			 {
  		    				 spinner1.setSelection(7);
  		    			 }
   		    			 else if (condition[0].equals("Stressed"))
  		    			 {
  		    				 spinner1.setSelection(8);
  		    			 }
   		    			 else
   		    				 spinner1.setSelection(0);
   		    			 
   		    			 
   		    			 checkSp2(condition[1]);
   		    			 break;
   		    		 }
 		    		
 		    	 }
 		    	 else
		    		exist=false;
 		    	 i++;
 	             } while (cursor.moveToNext());
 		      
 	        }
	      	db.close();
	} 
	  
	 public void checkSp2(String con){
		 spinner2 = (Spinner) findViewById(R.id.spinner2);
		 	 if (con.equals("Seizure type1"))
			 {
				 spinner2.setSelection(1);
			 }
			 else if (con.equals("Seizure type2"))
			 {
				 spinner2.setSelection(2);
			 }
			 else if (con.equals("Seizure type3"))
			 {
				 spinner2.setSelection(3);	 
			 }
			 else
				 spinner2.setSelection(0);
	 }
	
}
