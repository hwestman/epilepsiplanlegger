/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package no.mha.epilepsi.Activities;

import no.mha.epilepsi.services.Monitor;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;
import java.util.Locale;
import no.mha.epilepsi.R;

/**
 *
 * @author wbserver
 */
public class MonitorReminder extends Activity{
    TimePicker mTimePicker;
    
    
    public static MonitorReminder monitorReminder;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.monitordialog);
        monitorReminder = this;
        
        EpilepsiPlanlegger.getInstanceOf().alarmUser("Are you okey?");
        
        Monitor.REMINDER_STARTED = true;
        
        
        
        mTimePicker = (TimePicker)findViewById(R.id.timepicker);
        mTimePicker.setIs24HourView(true);
        mTimePicker.setCurrentHour(Monitor.REMINDERINTERVAL_MINUTE);
        mTimePicker.setCurrentMinute(Monitor.REMINDERINTERVAL_SECOND);
        
        //TODO INTERNATIONALIZATION
        //setTitle("Vil du fortsette å overvåke?");
        Button monitor_continue = (Button) findViewById(R.id.monitor_continue);
        monitor_continue.setOnClickListener(new OKListener());
        Button monitor_finish = (Button) findViewById(R.id.monitor_finish);
        monitor_finish.setOnClickListener(new OKListener());
    
    }
    public static MonitorReminder getInstanceOf(){
        return monitorReminder;
    }
    /*
     * dont need this yet
     */
    public void onInit(int i) {
    }
    
    /*
     * userdialog options
     */
    private class OKListener implements android.view.View.OnClickListener {
        @Override
        public void onClick(View v) {
            
            switch(v.getId()){
                
                case R.id.monitor_continue:{
                    Monitor.REMINDERINTERVAL_MINUTE = mTimePicker.getCurrentHour();
                    Monitor.REMINDERINTERVAL_SECOND = mTimePicker.getCurrentMinute();
                    Monitor.CONTINUE_PRESSED = true;
                    Monitor.reminderTimer();
                    
                    break;
                }
                case R.id.monitor_finish:{
                    Monitor.stopMonitor();
                   
                    
                    break;
                }
                
                    
            }
            
            finishIt();
        }
    }
    /*
     * closes the dialog for the warncontact activity
     */
    public static void finishIt(){
        getInstanceOf().finish();
        getInstanceOf().onDestroy();
    }
    
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
    
    }
    
    
    
    /*
     * user should not be able to do this
     */
    @Override
    public void onBackPressed (){
     
        
    }
    
    
  
    
}
