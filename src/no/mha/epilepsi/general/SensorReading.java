/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package no.mha.epilepsi.general;

import android.hardware.SensorEvent;
import android.net.Uri;
import android.os.Bundle;
import java.security.Timestamp;
import java.util.Calendar;

/**
 *
 * @author hallvardwestman
 */
public class SensorReading {
    
    java.sql.Timestamp timestamp;
    
    Bundle acceleration;
    
    Double decibel;
    
    Uri location;
    
     public SensorReading(){
        acceleration = null;
        decibel = null;
        location = null;
        timestamp = null;
            
    }
    public SensorReading(Bundle accel,Double deci,Uri loca){
        acceleration = accel;
        decibel = deci;
        location = loca;
        timestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
            
    }
   
    public Bundle getAcceleration() {
        return acceleration;
    }

    public Double getDecibel() {
        return decibel;
    }

    public Uri getLocation() {
        return location;
    }

    public java.sql.Timestamp getTimeStamp() {
        return timestamp;
    }
    
    
    
    
}
